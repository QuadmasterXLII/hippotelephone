import flask

images = []

app = flask.Flask(__name__)

@app.route("/image", methods=["GET", "POST"])
def image():
    if flask.request.method == "GET":
        return images[-1]
    else:
        images.append(flask.request.values.get("image"))
        return "yey"
        
@app.route('/')
def root():
    return app.send_static_file('index.php.html')

app.run()
